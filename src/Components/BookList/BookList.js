import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import getRandomUUID from 'uuid/v4';
import { Book } from '../Book';
import { Loader } from '../Loader';

const ListContainer = styled.main`
  background-color: #f5f5f5;
`;

const List = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 100px 0;
  margin: auto;
  max-width: 1250px;
`;

const BookList = ({ isLoading, books }) => {
  if (!books.length) return null;
  
  const extractedDataBooks = () => {
    return books.map((book) => {
      const bookCover = book.volumeInfo.imageLinks
        ? book.volumeInfo.imageLinks.thumbnail.replace('zoom=1', 'zoom=2')
        : null;
      const bookDescription = book.volumeInfo.description || 'This book has no description';

      return {
        id: getRandomUUID(),
        bookCover,
        title: book.volumeInfo.title,
        bookDescription,
      };
    });
  };

  return (
    <ListContainer>
      <List>
        {extractedDataBooks().map((book) => (<Book key={book.id} book={book} />))}
        {isLoading && <Loader position="fixed" top="70%"/>}
      </List>
    </ListContainer>
  );
};

export { BookList };

BookList.propTypes = {
  isLoading: PropTypes.bool,
  books: PropTypes.arrayOf(PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.object,
    ])
  )),
};

BookList.defaultProps = {
  books: null,
};
