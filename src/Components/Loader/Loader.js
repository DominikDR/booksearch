import React from 'react';
import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const Container = styled.div`
  position: ${props => props.position || 'absolute'};
  top: ${props => props.top || '20%'};
  left: 50%;
  right: 50%;
  font-size: 10px;
  margin: 50px auto;
  text-indent: -9999em;
  width: 11em;
  height: 11em;
  border-radius: 50%;
  background: #0dc5c1;
  background: linear-gradient(to right, #0dc5c1 10%, rgba(255, 255, 255, 0) 42%);
  animation: ${rotate} 1.4s infinite linear;
  transform: translateZ(0);
  &:before {
    content: '';
    width: 50%;
    height: 50%;
    background: #0dc5c1;
    border-radius: 100% 0 0 0;
    position: absolute;
    top: 0;
    left: 0;
  }
  &:after {
    content: '';
    background: #fff;
    width: 75%;
    height: 75%;
    border-radius: 50%;
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }
`;

const Loader = (props) => (
  <Container {...props} />
);

export { Loader };
