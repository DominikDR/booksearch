import React, { useMemo, useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { TextField, MenuItem, Button } from '@material-ui/core';
import { selectLangOptions } from './selectLangOptions';

const SearchContainer = styled.div`
  margin: auto;
  padding-top: 50px;
  height: 200px;
  background-color: #fff;
`;

const StyledForm = styled.form`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
`;

const StyledTextField = styled(TextField)`
  &.MuiFormControl-marginNormal {
    margin: 16px 10px 8px;
  }
  .MuiSelect-select {
    min-width: 200px;
  }
  .MuiInputBase-root input, .MuiSelect-select:focus {
    background-color: #fff;
  }
  &.MuiInputBase-root {
    border-radius: 4;
    background-color: #fcfcfb;
  }
  
  .MuiOutlinedInput-root {
    fieldset {
      border: 1px solid #e2e2e1;
      border-radius: 4px;
    }
    &.Mui-focused fieldset {
      box-shadow: rgba(25, 118, 210, 0.25) 0 0 0 2px;
      border-color: #1976d2;
      transition: box-shadow 300ms ease-out 0ms;
    }
  }
`;

const useStyles = makeStyles({
  button: {
    backgroundColor: '#cdc3ff',
    height: '52px',
    width: '100px',
    margin: '12px 10px 8px',
    '&:hover': {
      backgroundColor: '#9a8ce2',
    },
  },
  textField: {
    margin: '16px',
    marginBottom: '8px',
    backgroundColor: '#fff',
    '&:focus .Mui-focused fieldset': {
      boxShadow: 'rgba(25, 118, 210, 0.25) 0 0 0 2px',
      borderColor: '#1976d2',
      transition: 'box-shadow 300ms ease-out 0ms',
    },
  },
  formControl: {
    minWidth: '200px',
  },
});

const Search = React.memo(({ handleSubmitSearch }) => {
  const classes = useStyles();
  const [enteredValuesObject, setEnteredValue] = useState({
    title: '',
    author: '',
    language: '',
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    handleSubmitSearch(enteredValuesObject);
  };

  const handleInputChange = (name) => (event) => {
    setEnteredValue({
      ...enteredValuesObject,
      [name]: event.target.value,
    });
  };

  const selectOptions = useMemo(
    () => selectLangOptions.map((option) => (
      <MenuItem key={`${option.value} ${option.label}`} value={option.value}>{option.label}</MenuItem>
    )),
    [],
  );

  return (
    <SearchContainer>
      <StyledForm onSubmit={handleSubmit}>
        <StyledTextField
          id="book-title-input"
          label="Title"
          value={enteredValuesObject.title}
          onChange={handleInputChange('title')}
          type="text"
          margin="normal"
          variant="outlined"
        />
        <StyledTextField
          id="book-author-input"
          label="Author"
          value={enteredValuesObject.author}
          onChange={handleInputChange('author')}
          type="text"
          margin="normal"
          variant="outlined"
        />
        <StyledTextField
          id="book-language-select"
          label="Book Language"
          value={enteredValuesObject.language}
          onChange={handleInputChange('language')}
          type="select"
          select
          margin="normal"
          variant="outlined"
        >
          {selectOptions}
        </StyledTextField>
        <Button
          className={classes.button}
          variant="contained"
          type="submit"
        >
          Search
        </Button>
      </StyledForm>
    </SearchContainer>
  );
});

export { Search };

Search.propTypes = {
  handleSubmitSearch: PropTypes.func.isRequired,
};
