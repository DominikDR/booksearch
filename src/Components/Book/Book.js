import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import noPicturevAilable from '../../assets/images/noPicturevAilable.png';

const Card = styled.div`
  display: flex;
  max-width: 400px;
  height: 200px;
  margin: 8px;
  background-color: #fff;
  box-shadow: 0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12);
`;

const Image = styled.img`
  height: 200px;
  width: 150px;
  display: block;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

const TextBox = styled.div`
  height: 200px;
  width: 250px;
`;

const TextTitle = styled.div`
  font-size: 18px;
  padding: 10px;
  text-align: left;
  text-decoration: none;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  word-break: break-word;
  &:active, &:hover {
    overflow: visible;
    span {
      position: relative;
      background-color: #fff;
      padding: 3px;
      margin-left: -3px;
      box-shadow: 0px 3px 15px 4px rgba(0,0,0,0.16);
      transition: 0.2s linear;
    }
  }
`;

const TextDescription = styled.p`
  position: relative;
  font-size: 14px;
  padding: 10px;
  max-height: 55px;
  text-decoration: none;
  text-overflow: ellipsis;
  overflow: hidden;
  word-break: break-word;

  &:after {
    content: '';
    position: absolute;
    left: 160px;
    margin-left: 0px;
    width: 80px;
    height: 20px;
    top: 59px;
    background: linear-gradient(to right,rgba(240, 244, 245, 0.05),rgb(255, 255, 255));
  }
`;

const Book = ({
  book: {
    id,
    bookCover,
    title,
    bookDescription,
  },
}) => (
  <Card key={id}>
    <Image
      src={bookCover || noPicturevAilable}
      alt={`Book cover of ${title}`}
    />
    <TextBox>
      <TextTitle><span>{title}</span></TextTitle>
      <TextDescription>{bookDescription}</TextDescription>
    </TextBox>
  </Card>
);

export { Book };

Book.propTypes = {
  book: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.object,
    ])
  ),
};

Book.defaultProps = {
  book: null,
};
