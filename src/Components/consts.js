const URL = 'https://www.googleapis.com/books/v1/volumes?';
const PARTIAL_REQUEST_URL = 'items(id,volumeInfo/description,volumeInfo/title,volumeInfo/imageLinks/thumbnail)';
const MAX_RESULTS_TO_FETCH = 10;
const OFFSET_START_INDEX = 10;
const CALLBACK_DELAY_IN_MILISECONDS = 400;

export {
  URL,
  PARTIAL_REQUEST_URL,
  MAX_RESULTS_TO_FETCH,
  OFFSET_START_INDEX,
  CALLBACK_DELAY_IN_MILISECONDS
};
