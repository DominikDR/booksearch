import { useState } from 'react';

export const useFetchData = () => {
  const [data, setData] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const submitFetch = async (url) => {
    setIsLoading(true);
    setIsError(false);
    try {
      const response = await fetch(url, {
        method: 'get',
      });
      const dataFetched = await response.json();
      setData([
        ...dataFetched.items,
      ]);
    } catch (err) {
      console.error(err.message);
      setIsError(true);
    } finally {
      setIsLoading(false);
    }
  };

  return [{ data, isLoading, isError }, submitFetch];
};
