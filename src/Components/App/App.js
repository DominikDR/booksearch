import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import throttle from 'lodash.throttle';
import { Search } from '../Search';
import { BookList } from '../BookList';
import { Loader } from '../Loader';
import { useFetchData } from './useFetchData';
import { createUrl } from './auxiliary';
import { OFFSET_START_INDEX, CALLBACK_DELAY_IN_MILISECONDS } from '../consts';

const AppContainer = styled.div`
  min-height: 100vh;
`;

const BadMatch = styled.div`
  text-align: center;
`;

const App = () => {
  const [{ data, isLoading, isError }, doFetch] = useFetchData();
  const [books, setBooks] = useState([]);
  const [searchedValues, setSearchedValues] = useState(null);
  const [startIndex, setStartIndex] = useState(0);

  const fetchData = (searchedValuesObject) => {
    setBooks([]);
    setSearchedValues(searchedValuesObject);
    const url = createUrl(searchedValuesObject);
    doFetch(url);
  };

  useEffect(() => {
    const handleScroll = throttle(() => {
      const positionY = window.innerHeight + window.scrollY;
      const bodyHeight = document.body.offsetHeight;
      const pixelsFromWindowBottomToBottom = bodyHeight - positionY;
      if (pixelsFromWindowBottomToBottom <= 500) {
        setStartIndex((prevIndex) => prevIndex + OFFSET_START_INDEX);
      };
    }, CALLBACK_DELAY_IN_MILISECONDS);

    window.addEventListener('scroll', handleScroll);

    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  useEffect(() => {
    if (!data) return;
    setBooks(prevData => [ ...prevData, ...data ]);
  }, [data]);

  useEffect(() => {
    if (!data) return;
    doFetch(createUrl({
      ...searchedValues,
      startIndex,
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [startIndex]);

  return (
    <AppContainer>
      <Search handleSubmitSearch={fetchData} />
      {isError && <div>Server does not answer. Please try again</div>}
      {isLoading && !books.length && <Loader />}
      {data && !data.length && <BadMatch>Your search did not match any books. Try different keywords</BadMatch>}
      {<BookList isLoading={isLoading} books={books || data} />}
    </AppContainer>
  );
};

export { App };
