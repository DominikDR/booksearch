import {
  URL,
  PARTIAL_REQUEST_URL,
  MAX_RESULTS_TO_FETCH,
} from '../consts';

const createQuery = (parametersObject, wordJoinString, parametersJoinString) => {
  return Object.entries(parametersObject)
    .filter((value) => value[1] !== undefined && value[1] !== '')
    .map((value) => value.join(wordJoinString))
    .join(parametersJoinString);
};

const createUrl = (objectToParse) => {
  const {
    title,
    author,
    language,
    startIndex,
  } = objectToParse;

  const params = {
    q: createQuery({
      intitle: title,
      inauthor: author,
    }, ':', '+'),
    langRestrict: language,
    fields: PARTIAL_REQUEST_URL,
    maxResults: MAX_RESULTS_TO_FETCH,
    startIndex,
  };
  
  const finalQuery = createQuery(params, '=', '&');

  return `${URL}${finalQuery}`;
};

export {
  createUrl,
};
